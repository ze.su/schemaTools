import './components/qapp.tag';
import 'yo/lib/core/reset.scss';
import 'yo/usage/element/yo-checked.scss';
<app>
    <div style="height: 0.2rem; padding-top: 0.15rem;">
        <div class="yo-checked yo-checked-c">
            <input type="checkbox" name="userFlag" checked="{checked}" onchange="{userFlag}">
            <span class="type"></span>
        </div>
        打开上次配置
    </div>
    <qapp title="hy-transparent" style="transform:scale(.87) translateY(0);transition: all .5s;"></qapp>
    <qapp title="hy-none" style="transform:scale(.9) translateY(60px);transition: all .5s;"></qapp>
    <qapp title="hy-normal" style="transform:scale(.93) translateY(120px);transition: all .5s;"></qapp>
    <qapp title="webVC" style="transform:scale(.96) translateY(180px);transition: all .5s;"></qapp>
    <qapp title="defaultQR" style="transform:scale(1) translateY(240px);transition: all .5s;"></qapp>
    <script>
        this.on('mount', ()=>{
            var userFlag = localStorage.getItem("userFlag");
            var bFlag = (userFlag && userFlag != "false");
            this.update({
                checked: bFlag
            });
            if (bFlag) {
                this.userOption();
            }
        });

        this.userFlag = function() {
            var userFlag = this.root.querySelector("input[name=userFlag]").checked;
            console.log(userFlag);
            localStorage.setItem("userFlag", userFlag);
        };

        this.userOption = function() {
            var viewTitle = localStorage.getItem("lastSaveTitle");
            riot.route(viewTitle);
            this.onShow(this, viewTitle);
            return true;
        };

        this.onShow = function(opt, title) {
            var currTag = opt.tags['qapp'].filter((t)=>t.title===title);
            currTag.forEach((t)=>t.trigger('show'));
        };
    </script>
    <style scoped>
    :scope{
        width: 6rem;
        height: 6rem;
    }
    </style>
</app>
