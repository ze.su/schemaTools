import './header/header.tag';
import './loading/loading.tag';
import './qrcode/qrcode.tag';
import './list/list.tag';
import 'riot';
import { defaultqr,webvc,normal,transparent,none } from './webview.js';
import 'yo/lib/fragment/yo-list.scss';

<qapp>
    <yo_header></yo_header>
    <camel_loading if="{!show}" onclick={chosen}></camel_loading>
    <qrcode if="{show}" img="{qrcode}"></qrcode>
    <yo_list if="{show}" class="yo-list" ></yo_list>
    <script>
        var root = this.root;
        var self = this;
        this.title = opts.title;
        this.extra = null;
        this.chosen = function() {
            riot.route(this.title);
            this.trigger("save");
            this.parent.onShow(this.parent, this.title);
            return true;
        };

        this.close = function() {
            this.trigger("close");
        };

        this.on('show',()=>{
            this.beforeStyle = root.getAttribute("style");
            root.style.cssText += 'transform: scale(1) translateY(5%); z-index: 10;';
            this.update({show: true});
            this.webview();
        });

        this.on('close', ()=>{
            root.style.cssText = this.beforeStyle;
            this.update({show: false});
        });

        this.on('save', ()=>{
            console.log(this);
            localStorage.setItem("lastSaveTitle", this.title);
        });

        this.on('toggle',(e)=>{
            this.webview();
        });

        this.webview = function() {
            switch (this.title) {
                case 'defaultQR':
                    defaultqr(this);
                    break;
                case 'webVC':
                    webvc(this);
                    break;
                case 'hy-normal':
                    normal(this);
                    break;
                case 'hy-transparent':
                    transparent(this);
                    break;
                case 'hy-none':
                    none(this);
                    break;
                default:
            }
        };

    </script>
    <style scoped>
    :scope{
        overflow: hidden;
        position: relative;
    }
    </style>
</qapp>
